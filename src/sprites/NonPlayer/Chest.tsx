import {GameObjects, Physics} from 'phaser';

export type NonPlayerSprite = {
    id: string;
    makeActive: () => void;
    makeInactive: () => void;
} & GameObjects.GameObject

export class Chest extends Physics.Arcade.Image{
    coins = 10
    id = ''

    constructor(scene: Phaser.Scene,
                x: number, y: number,
                key: string | Phaser.Textures.Texture,
                frame: string | number | undefined,
                coins: number,
                id: string
    ) {
        super(scene, x, y, key, frame);
        this.coins = coins
        this.id = id

        this.scene.physics.world.enable(this)

        this.scene.add.existing(this)

        // Scale the chest game object
        this.setScale(2)
    }

    makeActive(){
        this.setActive(true)
        this.setVisible(true)
        this.body.checkCollision.none = false;
    }

    makeInactive() {
        this.setActive(false)
        this.setVisible(false)
        this.body.checkCollision.none = true;
    }
}