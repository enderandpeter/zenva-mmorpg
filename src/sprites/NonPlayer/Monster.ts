import {GameObjects, Physics, Scene, Textures} from 'phaser';

export class Monster extends Physics.Arcade.Image {
    private _id: string;
    private health: number;
    private healthBar: GameObjects.Graphics | undefined
    private maxHealth: number;

    constructor(
        scene: Scene,
        x: number,
        y: number,
        key: string | Textures.Texture,
        frame: string | number | undefined,
        id: string,
        health: number,
        maxHealth: number
    ) {
        super(scene, x, y, key, frame);
        this.scene = scene
        this._id = id
        this.health = health
        this.maxHealth = maxHealth

        // Enable Physics
        this.scene.physics.world.enable(this)
        // Set immovable if another object collides with our monster
        this.setImmovable(false);
        // scale our monster
        this.setScale(2);
        // collide with world bounds
        this.setCollideWorldBounds(true);

        // Add monster to existing scene
        this.scene.add.existing(this)

        // Update the origin
        this.setOrigin(0)

        this.createHealthBar()
    }

    createHealthBar() {
        this.healthBar = this.scene.add.graphics()
        this.updateHealthBar()
    }

    updateHealthBar(){
        this.healthBar!.clear()
        this.healthBar!.fillStyle(0xffffff, 1)// white, solid color
        this.healthBar!.fillRect(this.x, this.y - 8, 64, 5)
        this.healthBar!.fillGradientStyle(0xff0000,
            0xFF8E8E,
            0xff0000,
            0xFF8E8E,
            1,
            0.8,
            1,
            0.8
        )
        this.healthBar!.fillRect(this.x, this.y - 8, 64 * (this.health / this.maxHealth), 5)
    }

    updateHealth(health: number){
        this.health = health
        this.updateHealthBar()
    }

    update(){
        this.updateHealthBar()
    }

    makeActive() {
        this.setActive(true)
        this.setVisible(true)
        this.body.checkCollision.none = false;
        this.updateHealthBar()
    }

    makeInactive() {
        this.setActive(false)
        this.setVisible(false)
        this.body.checkCollision.none = true;
        this.healthBar!.clear()
    }

    get id() {
        return this._id
    }

    set id(value) {
        this._id = value
    }
}