import {Physics, Types, GameObjects, Input, Sound} from 'phaser';
import {Player} from "./Player";
import PlayerModel from "../../utilities/PlayerModel";

enum Direction {
    RIGHT = 'RIGHT',
    LEFT = 'LEFT',
    UP = 'UP',
    DOWN = 'DOWN'
}

export class PlayerContainer extends GameObjects.Container {
    private attackAudio: Phaser.Sound.BaseSound;
    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }
    private _id: string;
    get maxHealth(): number {
        return this._maxHealth;
    }
    private readonly _maxHealth: number;
    get health(): number {
        return this._health;
    }

    set health(value: number) {
        this._health = value;
    }
    private _health: number;
    private healthBar: GameObjects.Graphics | undefined
    velocity = 160
    currentDirection: Direction = Direction.RIGHT
    playerAttacking = false
    flipX = true
    swordHit = false
    player: Player | null = null
    weapon: GameObjects.Image
    constructor(scene: Phaser.Scene,
                x: number, y: number,
                key: string | Phaser.Textures.Texture,
                frame: string | number | undefined,
                health: number,
                maxHealth: number,
                id: string,
                attackAudio: Sound.BaseSound
    ) {
        super(scene, x, y);
        // set size on the container
        this.setSize(64, 64)

        // Enable Physics
        this.scene.physics.world.enable(this);

        (this.body as Physics.Arcade.Body).setCollideWorldBounds(true);

        // Add player container to existing scene
        this.scene.add.existing(this)

        // Camera will follow the player
        this.scene.cameras.main.startFollow(this)

        // Create the player
        this.player = new Player(this.scene, 0, 0, key, frame)
        this.add(this.player)

        // Create weapon game object
        this.weapon = this.scene.add.image(40, 0, 'items', 4);
        this.scene.add.existing(this.weapon)
        this.weapon.setScale(1.5);
        this.scene.physics.world.enable(this.weapon)
        this.add(this.weapon)
        this.weapon.alpha = 0

        this._health = health
        this._maxHealth = maxHealth
        this._id = id

        this.attackAudio = attackAudio

        // Create the player health bar
        this.createHealthBar()
    }

    createHealthBar() {
        this.healthBar = this.scene.add.graphics()
        this.updateHealthBar()
    }

    updateHealthBar(){
        this.healthBar!.clear()
        this.healthBar!.fillStyle(0xffffff, 1)// white, solid color
        this.healthBar!.fillRect(this.x - 32, this.y - 40, 64, 5)
        this.healthBar!.fillGradientStyle(0xff0000,
            0xFF8E8E,
            0xff0000,
            0xFF8E8E,
            1,
            0.8,
            1,
            0.8
        )
        this.healthBar!.fillRect(this.x - 32, this.y - 40, 64 * (this.health / this.maxHealth), 5)
    }

    updateHealth(health: number){
        this.health = health
        this.updateHealthBar()
    }

    update(cursors: Types.Input.Keyboard.CursorKeys) {
        const body = (this.body as Physics.Arcade.Body)
        body.setVelocity(0)

        if (cursors.left.isDown) {
            body.setVelocityX(-this.velocity);
            this.currentDirection = Direction.LEFT
            this.weapon.setPosition(-40, 0)
            this.player!.flipX = false
        } else if (cursors.right.isDown) {
            body.setVelocityX(this.velocity);
            this.currentDirection = Direction.RIGHT
            this.weapon.setPosition(40, 0)
            this.player!.flipX = true
        }

        if (cursors.up.isDown) {
            body.setVelocityY(-this.velocity);
            this.currentDirection = Direction.UP
            this.weapon.setPosition(0, -40)
        } else if (cursors.down.isDown) {
            body.setVelocityY(this.velocity);
            this.currentDirection = Direction.DOWN
            this.weapon.setPosition(0, 40)
        }

        // Only ran once for a single key press
        if(Input.Keyboard.JustDown(cursors.space) && !this.playerAttacking){
            this.weapon.alpha = 1
            this.playerAttacking = true
            this.attackAudio.play()
            this.scene.time.delayedCall(150, () => {
                this.weapon.alpha = 0
                this.playerAttacking = false
                this.swordHit = false
            }, [])
        }

        if (this.playerAttacking) {
            if(this.weapon.flipX){
                this.weapon.angle -= 10
            } else {
                this.weapon.angle += 10
            }
        } else {
            if (this.currentDirection === Direction.DOWN) {
                this.weapon.setAngle(-270)
            } else if (this.currentDirection === Direction.UP) {
                this.weapon.setAngle(-90)
            } else {
                this.weapon.setAngle(0)
            }
        }


        this.weapon.flipX = this.currentDirection === Direction.LEFT;

        this.updateHealthBar()
    }

    respawn(playerModel: PlayerModel) {
        this._health = playerModel.health
        this.setPosition(playerModel.x, playerModel.y)
        this.updateHealthBar()
    }
}