import { Physics, Types } from 'phaser';

export class Player extends Physics.Arcade.Image {
    constructor(scene: Phaser.Scene,
                x: number, y: number,
                key: string | Phaser.Textures.Texture,
                frame: string | number | undefined
    ) {
        super(scene, x, y, key, frame);

        // Enable Physics
        this.scene.physics.world.enable(this)
        this.setImmovable(true);
        this.setScale(2);

        this.scene.add.existing(this)
    }
}