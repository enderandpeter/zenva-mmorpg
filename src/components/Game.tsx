import {Types, Game as PhaserGame} from 'phaser';
import React, { Component } from 'react';
import appConfig from "../config";
import GameScene from "../scenes/Game";
import Boot from "../scenes/Boot";
import Title from "../scenes/Title";
import Ui from "../scenes/Ui";

const {debug} = appConfig;
export const config: Types.Core.GameConfig = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            debug,
            gravity: {
                y: 0
            }
        }
    },
    pixelArt: true,
    roundPixels: true,
    scene: [Boot, Title, GameScene, Ui],
    parent: 'game',
    title: 'Zenva MMORPG',
};

interface GameProps {
    initializeGame(game: PhaserGame | null):  void;
    game: PhaserGame | null;
}

export default class Game extends Component<GameProps> {
    componentDidMount(){
        this.props.initializeGame(new PhaserGame(config));
    }
    render(){
        return <div id="game"/>;
    }
}