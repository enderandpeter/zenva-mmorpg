import { GameObjects, Display } from 'phaser';

export default class Button extends GameObjects.Container {
    // @ts-ignore
    private button: GameObjects.Image;
    private buttonText: GameObjects.Text | undefined;
    private readonly key;
    private readonly text;
    private readonly targetCallback;
    private readonly hoverKey;
    constructor(
        scene: Phaser.Scene,
        x: number,
        y: number,
        key: string,
        hoverKey: string,
        text: string,
        targetCallback: () => void
    ) {
        super(scene, x, y);
        this.key = key;
        this.hoverKey = hoverKey;
        this.text = text;
        this.targetCallback = targetCallback;

        this.createButton();
        this.scene.add.existing(this);
    }

    private createButton() {
        this.button = this.scene.add.image(0, 0, 'button1')
        this.button.setInteractive();

        this.button.setScale(1.4);

        this.buttonText = this.scene.add.text(0, 0, this.text, { fontSize: '26px', color: '#fff'})
        Display.Align.In.Center(this.buttonText, this.button);

        this.add(this.button);
        this.add(this.buttonText)

        this.button.on('pointerdown', () => {
            this.targetCallback();
        })

        this.button.on('pointerover', () => {
            this.button.setTexture(this.hoverKey)
        })

        this.button.on('pointerout', () => {
            this.button.setTexture(this.key)
        })
    }
}