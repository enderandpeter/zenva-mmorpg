import {Scene, Sound, GameObjects, Physics} from 'phaser';
import {Chest, NonPlayerSprite} from "../sprites/NonPlayer/Chest";
import Map from "../utilities/Map";
import GameManager, {GameManagerMonsters} from "../utilities/GameManager";
import ChestModel from "../utilities/ChestModel";
import MonsterModel from "../utilities/MonsterModel";
import {Monster} from "../sprites/NonPlayer/Monster";
import {PlayerContainer} from "../sprites/Player/PlayerContainer";
import PlayerModel from "../utilities/PlayerModel";

export default class Game extends Scene {
    private player: PlayerContainer | undefined;
    private cursors: any;
    private chests: Physics.Arcade.Group | undefined;
    private monsters: Physics.Arcade.Group | undefined;
    private goldPickupAudio: Sound.BaseSound | undefined;
    private map: Map | object;
    private gameManager: GameManager | {};
    private playerAttackAudio: Sound.BaseSound | undefined;
    private playerDamageAudio: Sound.BaseSound | undefined;
    private playerDeathAudio: Sound.BaseSound | undefined;
    private monsterDeathAudio: Sound.BaseSound | undefined;

    constructor() {
        super('Game');
        this.gameManager = {}
        this.map = {}
    }

    init() {
        this.scene.launch('Ui')

    }

    create() {
        this.createMap()

        this.createAudio()

        this.createGroups()

        this.createInput()

        this.createGameManager();
    }

    update() {
        this.cursors && this.player?.update(this.cursors);
    }

    createAudio() {
        this.goldPickupAudio = this.sound.add('goldSound', {loop: false, volume: 0.3});
        this.playerAttackAudio = this.sound.add('playerAttack', {loop: false, volume: 0.01});
        this.playerDamageAudio = this.sound.add('playerDamage', {loop: false, volume: 0.2});
        this.playerDeathAudio = this.sound.add('playerDeath', {loop: false, volume: 0.2});
        this.monsterDeathAudio = this.sound.add('enemyDeath', {loop: false, volume: 0.2});
    }

    createPlayer(player: PlayerModel) {
        this.player = new PlayerContainer(
            this,
            player.x * 2,
            player.y * 2,
            'characters',
            0,
            player.health,
            player.maxHealth,
            player.id,
            this.playerAttackAudio!
        );
    }

    createGroups() {
        this.chests = this.physics.add.group()
        this.monsters = this.physics.add.group()
        this.monsters.runChildUpdate = true
    }

    spawnChest(chestModel: ChestModel) {
        let chest = this.chests?.getFirstDead();
        if (!chest) {
            chest = new Chest(this, chestModel.x * 2, chestModel.y * 2, 'items', 0, chestModel.gold, chestModel.id);
            this.chests?.add(chest)
        } else {
            chest.coins = chestModel.gold;
            chest.id = chestModel.id;
            chest.setPosition(chestModel.x * 2, chestModel.y * 2)
            chest.makeActive()
        }
    }

    spawnMonster(monsterModel: MonsterModel) {
        let monster = this.monsters?.getFirstDead();
        if (!monster) {
            monster = new Monster(
                this,
                monsterModel.x,
                monsterModel.y,
                'monsters',
                monsterModel.frame,
                monsterModel.id,
                monsterModel.health,
                monsterModel.maxHealth
            );
            // Add monsters to monsters group
            this.monsters?.add(monster)
        } else {
            monster.id = monsterModel.id;
            monster.health = monsterModel.health;
            monster.maxHealth = monsterModel.maxHealth;
            monster.setTexture('monsters', monsterModel.frame)
            monster.setPosition(monsterModel.x, monsterModel.y)
            monster.makeActive()
        }
    }

    createInput() {
        this.cursors = this.input.keyboard.createCursorKeys();
    }

    addCollisions() {
        // Collisions between player and blocked layer
        this.physics.add.collider(
            this.player as GameObjects.GameObject, (this.map as Map).blockedLayer as GameObjects.GameObject)
        // Check for overlaps between the player and chest game objects
        this.physics.add.overlap(
            this.player as GameObjects.GameObject, this.chests as Physics.Arcade.Group, this.collectChest,
            undefined, this);
        // Check for collisions between the monster group and the tiled blocked layer
        this.physics.add.collider(
            this.monsters as GameObjects.Group, (this.map as Map).blockedLayer as GameObjects.GameObject)
        // Check for overlaps between player's weapon and monster game objects
        this.physics.add.overlap(
            this.player?.weapon as GameObjects.GameObject, this.monsters as GameObjects.Group, this.enemyOverlap, undefined, this
        )
    }

    enemyOverlap(weapon: GameObjects.GameObject, enemy: GameObjects.GameObject) {
        const theEnemy = enemy as NonPlayerSprite
        if (this.player?.playerAttacking && !this.player.swordHit) {
            this.player.swordHit = true
            this.events.emit('monsterAttacked', theEnemy.id, this.player.id)
        }
    }

    collectChest(player: GameObjects.GameObject, chest: GameObjects.GameObject) {
        this.goldPickupAudio?.play();

        const theChest = chest as Chest
        const thePlayer = player as PlayerContainer

        // spawn a new chest
        this.events.emit('pickupChest', theChest.id, thePlayer.id);
    }

    private createMap() {
        this.map = new Map(this, 'map', 'background', 'background', 'blocked');
    }

    private createGameManager() {
        this.events.on('spawnPlayer', (player: PlayerModel) => {
            this.createPlayer(player)
            this.addCollisions()
        })

        this.events.on('chestSpawned', (chest: ChestModel) => {
            this.spawnChest(chest)
        })

        this.events.on('monsterSpawned', (monster: MonsterModel) => {
            this.spawnMonster(monster)
        })

        this.events.on('monsterRemoved', (monsterId: string) => {
            const theMonster = this.monsters?.getChildren().find((monster: GameObjects.GameObject) => {
                const theMonster = monster as Monster
                return theMonster.id === monsterId
            }) as Monster
            theMonster.makeInactive()
            this.monsterDeathAudio?.play()
        })

        this.events.on('updateMonsterHealth', (monsterId: string, health: number) => {
            const theMonster = this.monsters?.getChildren().find((monster: GameObjects.GameObject) => {
                const theMonster = monster as Monster
                return theMonster.id === monsterId
            }) as Monster
            theMonster.updateHealth(health)
        })

        this.events.on('monsterMovement', (monsters: GameManagerMonsters) => {
             this.monsters?.getChildren().forEach((monster: GameObjects.GameObject) => {
                Object.keys(monsters).forEach((monsterId) => {
                    const theMonster = monster as Monster
                    if(theMonster.id === monsterId){
                        this.physics.moveToObject(monster, monsters[monsterId], 40)
                    }
                })
            })
        })

        this.events.on('updatePlayerHealth', (playerContainerId: string, health: number) => {
            if(health < this.player!.health){
                this.playerDamageAudio?.play()
            }
           this.player?.updateHealth(health)
        })

        this.events.on('chestRemoved', (chestId: string) => {
            const theChest = this.chests?.getChildren().find((chest: GameObjects.GameObject) => {
                const theChest = chest as Chest
                return theChest.id === chestId
            }) as Chest
            theChest.makeInactive()
        })

        this.events.on('respawnPlayer', (player: PlayerModel) => {
            this.player?.respawn(player)
            this.playerDeathAudio?.play()
        })

        if (this.map instanceof Map) {
            this.gameManager = new GameManager(this, this.map.map?.objects);
            const gameManager = this.gameManager as GameManager;
            gameManager.setup();
        }
    }
}
