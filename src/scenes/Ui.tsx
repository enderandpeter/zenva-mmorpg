import { Scene, GameObjects } from 'phaser';

export default class Ui extends Scene {
    private scoreText: GameObjects.Text | undefined;
    private coinIcon: GameObjects.Image | undefined;
    private gameScene: Scene | undefined;

    constructor() {
        super('Ui');
    }

    init(){
        // Grab a reference to the Game Scene
        this.gameScene = this.scene.get('Game');
    }

    create() {
        this.setupUiElements()
        this.setupEvents()
    }

    setupUiElements() {
        // Create the score text game object
        this.scoreText = this.add.text(35, 8, 'Coins: 0', { fontSize: '16px', color: '#fff'})
        // create coin icon
        this.coinIcon = this.add.image(15, 15, 'items', 3)
    }

    setupEvents() {
        // Listen for the updateScore event from the Game Scene
        this.gameScene?.events.on('updateScore', (score: number) => {
            this.scoreText?.setText(`Coins: ${score}`);
        })
    }
}
