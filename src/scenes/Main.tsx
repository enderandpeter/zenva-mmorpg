import { Scene, Types } from 'phaser';

export default class Main extends Scene {
    private player: Types.Physics.Arcade.ImageWithDynamicBody | undefined;
    private cursors: any;
    private wall: Types.Physics.Arcade.ImageWithDynamicBody | undefined;
    private chest: any;
    preload(){
        this.load.image('button1', 'assets/images/ui/blue_button01.png');
        this.load.spritesheet('items', 'assets/images/items.png', {frameWidth:32, frameHeight:32});
        this.load.spritesheet('characters', 'assets/images/characters.png', {frameWidth:32, frameHeight:32});
        this.load.audio('goldSound', 'assets/audio/Pickup.wav');
    }
    update(){
        this.player?.setVelocity(0)

        if(this.cursors.left.isDown){
            this.player?.setVelocityX(-160);
        } else if(this.cursors.right.isDown){
        }

        if(this.cursors.up.isDown){
            this.player?.setVelocityY(-160);
        } else if(this.cursors.down.isDown){
            this.player?.setVelocityY(160);
        } else {

        }
    }
}