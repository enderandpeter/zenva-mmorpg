import { Scene, GameObjects } from 'phaser';
import Button from "../components/Ui/Button";

export default class Title extends Scene {
    private titleText: GameObjects.Text | undefined;
    private startGameButton: Button | undefined;
    constructor() {
        super('Title');
    }

    create() {
        this.titleText = this.add.text(this.scale.width / 2, this.scale.height / 2, 'Zenva MMORPG', { fontSize: '64px', color: '#fff'})
        this.titleText.setOrigin(0.5)

        // create the Play game button
        this.startGameButton = new Button(
            this,
            this.scale.width / 2,
            this.scale.height * 0.65,
            'button1',
            'button2',
            'Start',
            this.startScene.bind(this, 'Game')
        )
    }

    startScene(targetScene: string) {
        this.scene.start(targetScene)
    }
}
