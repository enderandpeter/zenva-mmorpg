import React, {Component} from 'react';
import './App.css';
import Phaser from 'phaser';
import Game from "./components/Game";

interface AppState {
  game: Phaser.Game | null;
}

class App extends Component<{}, AppState> {
  state = {game: null};

  initializeGame(game: Phaser.Game) {
    this.setState({game});
  }

  render() {
    return (
        <div className="App">
          <h1>{
            this.state.game ?
                //@ts-ignore
                this.state.game.config.gameTitle
                :
                ''
          }</h1>
          <Game
              game={this.state.game}
              initializeGame={this.initializeGame.bind(this)}
          />
        </div>
    );
  }
}

export default App;
