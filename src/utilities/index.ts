import {Location, TiledObject} from "./GameManager";

export default {
    MONSTER: 'MONSTER',
    CHEST: 'CHEST'
}

export const randomNumber = (min: number, max: number) => {
    return Math.floor(Math.random() * max) + min;
}

/**
 * Translate the Tiled coordinate (anchor point at bottom left corner of image) to the Phaser coordinate (default anchor point in middle of image)
 * @param obj
 */
export const tiledObjectPhaserPosition = (obj: TiledObject): Location => {
    return [obj.x + (obj.width / 2), obj.y - (obj.height / 2) ]
}