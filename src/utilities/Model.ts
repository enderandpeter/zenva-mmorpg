import { v4 as uuidv4 } from 'uuid';

export default class Model {
    public _id: string;
    private readonly _spawnerId: string;
    protected _x: number;
    protected _y: number;

    constructor(x: number, y: number, spawnerId: string) {
        this._id = `${spawnerId}-${uuidv4()}`;
        this._spawnerId = spawnerId;
        this._x = x;
        this._y = y;
    }

    get id(){
        return this._id
    }
    get x() {
        return this._x;
    }
    get y() {
        return this._y;
    }
    get spawnerId() {
        return this._spawnerId;
    }
}