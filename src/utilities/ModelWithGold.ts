import Model from "./Model";

export default class ModelWithGold extends Model{
    protected _gold: number;
    constructor(x: number, y: number, spawnerId: string, gold: number) {
        super(x, y, spawnerId)
        this._gold = gold;
    }

    get gold() {
        return this._gold;
    }
}