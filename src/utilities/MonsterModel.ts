import ModelWithGold from "./ModelWithGold";
import {randomNumber} from "./index";

export default class MonsterModel extends ModelWithGold{
    private readonly _frame: number
    private _health: number
    private readonly _attack: number
    private readonly _maxHealth: number
    constructor(x: number, y: number, spawnerId: string, gold: number, frame: number = 0, health: number = 0, attack: number = 0) {
        super(x * 2, y * 2, spawnerId, gold)
        this._frame = frame
        this._health = health
        this._maxHealth = health
        this._attack = attack
    }

    get frame(){
        return this._frame
    }
    get health(){
        return this._health
    }
    get maxHealth(){
        return this._maxHealth
    }
    get attack(){
        return this._attack
    }
    loseHealth(){
        this._health -= 1
    }
    move(){
        const randomPosition = randomNumber(1, 8)
        const distance = 64

        switch(randomPosition){
            case 1:
                this._x += distance
                break;
            case 2:
                this._x -= distance
                break;
            case 3:
                this._y += distance
                break;
            case 4:
                this._y -= distance
                break;
            case 5:
                this._x += distance
                this._y += distance
                break;
            case 6:
                this._x += distance
                this._y -= distance
                break;
            case 7:
                this._x -= distance
                this._y += distance
                break;
            case 8:
                this._x -= distance
                this._y -= distance
                break;
            default:
                break;
        }
    }
}