import TilemapLayer = Phaser.Tilemaps.TilemapLayer;
import Tileset = Phaser.Tilemaps.Tileset;
import Tilemap = Phaser.Tilemaps.Tilemap;
import {Scene} from "phaser";

export default class Map {
    private key: string;
    private tileSetName: string;
    private bgLayerName: string;
    scene: Scene | null = null;
    tiles: Tileset | undefined;
    backgroundLayer: TilemapLayer | undefined;
    map: Tilemap | undefined;
    blockedLayer: TilemapLayer | undefined;
    private blockedLayerName: string;
    constructor(scene: Scene, key: string, tileSetName: string, bgLayerName: string, blockedLayerName: string) {
        this.scene = scene;
        this.key = key; // Tiled JSON file key name
        this.tileSetName = tileSetName; // Tiled Tileset image key name
        this.bgLayerName = bgLayerName; // Name of the layer created in Tiled for the background
        this.blockedLayerName = blockedLayerName; // Name of the layer created in Tiled for the blocked (impassable) areas

        this.createMap();
    }

    private createMap() {
        const gameScene = this.scene as Scene;

        // create the tile map
        this.map = gameScene.make.tilemap({key: this.key})
        const map = this.map as Tilemap;
        // add tileset image to our map
        this.tiles = map.addTilesetImage(
            this.tileSetName,
            this.tileSetName,
            32,
            32,
            1,
            2
        );
        // create background layer
        this.backgroundLayer = map.createLayer(
            this.bgLayerName,
            this.tiles,
            0,
            0
        )
        this.backgroundLayer.setScale(2);
        // create blocked layer
        this.blockedLayer = map.createLayer(
            this.blockedLayerName,
            this.tiles,
            0,
            0
        );
        this.blockedLayer.setScale(2);

        this.blockedLayer.setCollisionByExclusion([-1]);

        gameScene.physics.world.bounds.width = map.widthInPixels * 2;
        gameScene.physics.world.bounds.height = map.heightInPixels * 2;

        // Limit the camera to the size of the map
        gameScene.cameras.main.setBounds(0, 0, map.widthInPixels * 2, map.heightInPixels * 2);
    }
}