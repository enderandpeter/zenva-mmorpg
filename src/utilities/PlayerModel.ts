import ModelWithGold from "./ModelWithGold";
import {Location} from "./GameManager";

export default class PlayerModel extends ModelWithGold{
    private _health: number = 10
    private readonly _maxHealth: number = 10
    spawnLocations: Location[] = []
    constructor(spawnLocations: Location[]) {
        super(0, 0, 'player', 10)
        this.spawnLocations = spawnLocations
        const location: Location = this.spawnLocations[Math.floor(Math.random() * this.spawnLocations.length)];
        [this._x, this._y] = location
    }

    updateGold(gold: number){
        this._gold += gold
    }

    updateHealth(health: number){
        this.health += health
        if(this.health > this._maxHealth){
            this.health = this._maxHealth
        }
    }

    respawn(){
        this.health = this.maxHealth
        const location: Location = this.spawnLocations[Math.floor(Math.random() * this.spawnLocations.length)];
        [this._x, this._y] = location
        this._x *= 2
        this._y *= 2
    }

    get health(){
        return this._health
    }
    set health(value){
        this._health = value
    }

    get maxHealth() {
        return this._maxHealth
    }
}