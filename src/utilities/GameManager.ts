import {Scene} from "phaser";
import Spawner, {SpawnerConfig} from "./Spawner";
import ChestModel from "./ChestModel";
import SpawnerTypes, {tiledObjectPhaserPosition} from "./index"
import MonsterModel from "./MonsterModel";
import PlayerModel from "./PlayerModel";

const {CHEST, MONSTER} = SpawnerTypes

export type Location = [number, number];

export interface LocationList {
    [spawnerId: string]: Location[]
}

export type GameManagerChests = { [id: string]: ChestModel }
export type GameManagerMonsters = { [id: string]: MonsterModel }
export type GameManagerPlayers = { [id: string]: PlayerModel }

export interface TiledObject {
    x: number;
    y: number;
    width: number;
    height: number;
    properties: { spawner: number }
}

export interface Layer {
    name: string;
    objects: TiledObject[]
}

export interface Model {
    id: string;
}

export interface SpawnersList {
    [id: string]: Spawner;
}

export default class GameManager {
    private scene: Phaser.Scene;
    private mapData: any;
    private spawners: SpawnersList;
    private chests: GameManagerChests;
    private monsters: GameManagerMonsters;
    private players: GameManagerPlayers;
    private playerLocations: Location[];
    private chestLocations: LocationList;
    private monsterLocations: LocationList;

    constructor(scene: Scene, mapData: any) {
        this.scene = scene;
        this.mapData = mapData;
        this.spawners = {};
        this.chests = {};
        this.monsters = {};
        this.players = {};
        this.playerLocations = [];
        this.chestLocations = {};
        this.monsterLocations = {};
    }

    private parseMapData() {
        this.mapData.forEach((layer: Layer) => {
            if (layer.name === 'player_locations') {
                layer.objects.forEach((obj: TiledObject) => {
                    this.playerLocations.push([...tiledObjectPhaserPosition(obj)])
                })
            } else if (layer.name === 'chest_locations') {
                layer.objects.forEach((obj: TiledObject) => {
                    if (this.chestLocations[obj.properties.spawner]) {
                        this.chestLocations[obj.properties.spawner].push([...tiledObjectPhaserPosition(obj)])
                    } else {
                        this.chestLocations[obj.properties.spawner] = [[...tiledObjectPhaserPosition(obj)]];
                    }
                })
            } else if (layer.name === 'monster_locations') {
                layer.objects.forEach((obj: any) => {
                    if (this.monsterLocations[obj.properties.spawner]) {
                        this.monsterLocations[obj.properties.spawner].push([...tiledObjectPhaserPosition(obj)])
                    } else {
                        this.monsterLocations[obj.properties.spawner] = [[...tiledObjectPhaserPosition(obj)]];
                    }
                })
            }
        })
        console.log(this.playerLocations);
        console.log(this.chestLocations);
        console.log(this.monsterLocations);
    }

    private setupEventListener() {
        this.scene.events.on('pickupChest', (chestId: string, playerContainerId: string) => {
            // Update the spawner
            if (this.chests[chestId]) {
                const { gold } = this.chests[chestId]

                // updating the player's gold
                this.players[playerContainerId].updateGold(gold)
                this.scene.events.emit('updateScore', this.players[playerContainerId].gold);

                // Removing the chest
                this.spawners[this.chests[chestId].spawnerId].removeObject(chestId)
                this.scene.events.emit('chestRemoved', chestId);
            }
        })

        this.scene.events.on('monsterAttacked', (monsterId: string, playerContainerId: string) => {
            const player = this.players[playerContainerId];
            const monster = this.monsters[monsterId]
            // Update the spawner
            if (monster) {
                // Subtract health from monster model
                monster.loseHealth()

                const { gold, attack } = monster

                // Check monster's health. If dead, remove the object
                if(monster.health <= 0){
                    // updating the player's gold
                    player.updateGold(gold)
                    this.scene.events.emit('updateScore', player.gold);

                    // Removing the monster
                    this.spawners[this.monsters[monsterId].spawnerId].removeObject(monsterId)
                    this.scene.events.emit('monsterRemoved', monsterId);

                    // Add bonus health to player
                    player.updateHealth(2)
                    this.scene.events.emit('updatePlayerHealth', playerContainerId, player.health)
                } else {
                    // update player's health
                    player.updateHealth(-attack)

                    // Update player health
                    this.scene.events.emit('updatePlayerHealth', playerContainerId, player.health)

                    // Update the monster's health
                    this.scene.events.emit('updateMonsterHealth', monsterId, monster.health)

                    // Check the player's health
                    // If below 0, the player respawns
                    if(player.health <= 0){
                        // Update the gold the player has
                        player.updateGold( Math.floor(-player.gold / 2))
                        this.scene.events.emit('updateScore', player.gold);

                        // Respawn the player
                        player.respawn()
                        this.scene.events.emit('respawnPlayer', player);
                    }
                }
            }
        })
    }

    private setupSpawners() {
        const config: SpawnerConfig = {
            spawnInterval: 3000,
            limit: 3,
            spawnerType: CHEST,
            id: ''
        }

        let spawner

        // Create chest spawners
        Object.keys(this.chestLocations).forEach((key) => {
            config.id = `chest-${key}`

            // Feel free to leave the parameters on one line. This is for readability
            spawner = new Spawner(
                config,
                this.chestLocations[key],
                // @ts-ignore
                this.addChest.bind(this),
                this.deleteChest.bind(this),
                this.moveMonsters.bind(this)
            );

            this.spawners[spawner.id] = spawner;
        });

        // Create Monster Spawners
        Object.keys(this.monsterLocations).forEach((key) => {
            config.id = `monster-${key}`
            config.spawnerType = MONSTER

            // Feel free to leave the parameters on one line. This is for readability
            spawner = new Spawner(
                config,
                this.monsterLocations[key],
                // @ts-ignore
                this.addMonster.bind(this),
                this.deleteMonster.bind(this),
                this.moveMonsters.bind(this)
            );

            this.spawners[spawner.id] = spawner;
        });
    }

    private spawnPlayer() {
        const player = new PlayerModel(this.playerLocations)
        this.players[player.id] = player
        this.scene.events.emit('spawnPlayer', player);
    }

    setup() {
        this.parseMapData();
        this.setupEventListener();
        this.setupSpawners();
        this.spawnPlayer();
    }

    addChest(chestId: string, chest: ChestModel) {
        this.chests[chestId] = chest;
        this.scene.events.emit('chestSpawned', chest);
    }

    deleteChest(id: string) {
        delete this.chests[id];
    }

    addMonster(monsterId: string, monster: MonsterModel) {
        this.monsters[monsterId] = monster;
        this.scene.events.emit('monsterSpawned', monster);
    }

    deleteMonster(monsterId: string) {
        delete this.monsters[monsterId]
    }

    moveMonsters(){
        this.scene.events.emit('monsterMovement', this.monsters)
    }
}