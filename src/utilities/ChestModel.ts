import ModelWithGold from "./ModelWithGold";

export default class ChestModel extends ModelWithGold{
    constructor(x: number, y: number, spawnerId: string, gold: number) {
        super(x, y, spawnerId, gold)
    }
}