import ChestModel from "./ChestModel";
import {Location} from "./GameManager";
import SpawnerTypes, {randomNumber} from './index';
import MonsterModel from "./MonsterModel";

const { CHEST, MONSTER } = SpawnerTypes;

export interface SpawnerConfig {
    id: string;
    spawnInterval: number;
    spawnerType: string;
    limit: number;
}

export type AddObject =  (id: string, chest: ChestModel) => void;

export default class Spawner {
    private _id: any;
    spawnInterval: any;
    private readonly limit: any;
    private objectType: any;
    private spawnLocations: Location[];
    private addObject: AddObject;
    private deleteObject: any;
    private objectsCreated: any[];
    private interval: number | undefined;
    private moveMonsterInterval: number | undefined;
    private moveObjects: () => void;
    constructor(
        config: SpawnerConfig,
        spawnLocations: Location[],
        addObject: () => void,
        deleteObject: () => void,
        moveObjects: () => void
    ) {
        this._id = config.id;
        this.spawnInterval = config.spawnInterval;
        this.limit = config.limit;
        this.objectType = config.spawnerType;
        this.spawnLocations = spawnLocations;
        this.addObject = addObject;
        this.deleteObject = deleteObject;
        this.moveObjects = moveObjects

        this.objectsCreated = [];
        this.start();
    }

    get id(){
        return this._id
    }

    start(){
        this.interval = window.setInterval(() => {
            if (this.objectsCreated.length < this.limit) {
                this.spawnObject();
            }
        }, this.spawnInterval);

        if(this.objectType === MONSTER){
            this.moveMonsters()
        }
    }

    removeObject(id: string) {
        this.objectsCreated = this.objectsCreated.filter(obj => obj.id !== id)
        this.deleteObject(id)
    }

    private spawnObject() {
        if(this.objectType === CHEST){
            this.spawnChest();
        } else if(this.objectType === MONSTER){
            this.spawnMonster();
        }
    }

    private spawnChest() {
        const location = this.pickRandomLocation();
        const chest = new ChestModel(location[0]!, location[1]!, this.id, randomNumber(1, 10));
        this.objectsCreated.push(chest);
        this.addObject(chest.id, chest);
    }

    private spawnMonster() {
        const location = this.pickRandomLocation();
        const monster = new MonsterModel(
            location[0]!,
            location[1]!,
            this.id,
            randomNumber(1, 10),
            randomNumber(0, 20),
            randomNumber(3, 5),
            1
        );
        this.objectsCreated.push(monster);
        this.addObject(monster.id, monster);
    }

    private pickRandomLocation(): Location {
        const location: Location = this.spawnLocations[Math.floor(Math.random() * this.spawnLocations.length)]
        const invalidLocation = this.objectsCreated.some((obj) => {
            return obj.x === location[0] && obj.y === location[1];
        });

        if(invalidLocation) {
            return this.pickRandomLocation();
        }

        return location;
    }

    moveMonsters(){
        this.moveMonsterInterval = window.setInterval(() => {
            this.objectsCreated.forEach((monster) => {
                monster.move()
            })
            this.moveObjects()
        }, 1000);
    }
}